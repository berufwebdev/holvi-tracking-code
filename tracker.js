
// Denne kode skal placeres sidst i kildekoden på alle sider før </body> slut tag
// Beruf 8/12 2014

<script type="text/javescript">
	jQuery(document).ready(function($) {

	  	var typed = false;

	  	$('form input').not('.language-switcher-multi form input, .plugin-invite-me-countryselect-form input').focus(function(){
			typed = false;
			var name = $(this).attr('name');
			var formName = $(this).parents('form:first').attr('id');
			ga('send', 'event', 'form field', 'activate', formName); 
			//console.log('%cim in the box called ' + name + ' on ' + formName, 'color: red;'); // Debugging	
		});	

		$('form input').not('.language-switcher-multi form input, .plugin-invite-me-countryselect-form input').keypress(function(){
			if (typed == false ) {
				var name = $(this).attr('name');
				var formName = $(this).parents('form:first').attr('id');
				ga('send', 'event', 'form field', 'type', formName); 
				typed = true;
				//console.log('%cim typing in the box called ' + name + ' on ' + formName, 'color: blue;'); // Debugging
			}
		});

		$('form').not('.language-switcher-multi form, .plugin-invite-me-countryselect-form input').submit(function(event) {
			event.preventDefault();
			var formName = $(this).attr('id');
			ga('send', 'event', 'form', 'submit', formName); 
			//console.log('Submit on ' + formName); // Debugging
		});

	});
</script>